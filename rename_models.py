import tensorflow as tf
import numpy as np
import os, argparse
from neural_net.model import model

training_args = np.load(os.path.join('model', 'typo', 'experiment-configuration.npy')).item()['args']
dictionary = np.load(os.path.join('model', 'typo', 'translate_dict.npy')).item()

in_seq_length = training_args.max_prog_length
out_seq_length = training_args.max_fix_length
vocab_size = len(dictionary)

# Create the old model
seq2seq = model(training_args.max_prog_length, training_args.max_fix_length + 1, len(dictionary)+1,
                rnn_cell='GRU', memory_dim=300, num_layers=4, dropout=0,
                embedding_dim=50, bidirectional=False, trainable=False, scope=None)

saver = tf.train.Saver()
sess  = tf.Session()
saver.restore(sess, "model/typo/ckpt")

# create a dictionary of variables in the old scope indexed by name
old_vars = {'/'.join(v.name.split('/')[1:]) : v
            for v in tf.all_variables()
            if v.name.startswith("embedding_attention_seq2seq/")}

# Create the new model
seq2seq = model(training_args.max_prog_length, training_args.max_fix_length + 1, len(dictionary)+1,
                rnn_cell='GRU', memory_dim=300, num_layers=4, dropout=0,
                embedding_dim=50, bidirectional=False, trainable=False, scope='typo')

assignments = [vNew.assign(old_vars['/'.join(vNew.name.split('/')[1:])])
               for vNew in tf.all_variables()
               if vNew.name.startswith("typo/")]

# Run the assingment ops
for assignment in assignments:
    sess.run(assignment)

saver2 = tf.train.Saver([vNew for vNew in tf.all_variables()
                         if vNew.name.startswith("typo/")])

saver2.save(sess, "model/typo/checkpoint_renamed")
