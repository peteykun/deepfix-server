from util.tokenizer import Tokenizer
import os
from util.helpers import get_lines
from data_processing.global_values import get_max_line_length

class Memoized_Tokenizer(Tokenizer):
    def tokenize(self, code, keep_format_specifiers=False, keep_names=True, \
                 keep_literals=False, dataset=None, filename=None):
        if keep_format_specifiers != False:
            raise Exception
        
        if keep_literals != False:
            raise Exception
        
        if dataset is None or filename is None:
            print dataset
            print filename
            raise Exception
        
        try:
            with open(os.path.join('data', 'tokenized_source', dataset, filename), 'r+') as f:
                result = f.read()
        except OSError:
            raise
        
        if not keep_names:
            result_new = ''
            
            for token in result.split():
                if '_<id>_' not in token:
                    result_new += token + ' '
                else:
                    result_new += '_<id>_@ '
            
            result = result_new.rstrip()
            
        result = self.enforce_restrictions(result)
        
        return result, {}, []
    