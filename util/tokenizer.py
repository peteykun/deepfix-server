from abc import abstractmethod
from util.helpers import get_lines

class UnexpectedTokenException(Exception):
    pass

class EmptyProgramException(Exception):
    '''In fn tokenizer:get_lines(), positions are empty, most probably the input program \
       is without any newline characters or has a special character such as ^A'''
    pass

class Tokenizer:

    @abstractmethod
    def tokenize(self, code, keep_format_specifiers=False, keep_names=True, \
                 keep_literals=False):
        return NotImplemented
    
    def enforce_restrictions(self, result):
        return result
    
