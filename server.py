from offline import DeepFix
from bottle import run, post, request, response, get, route, static_file
import json

deepfix = DeepFix()

@route('/')
def index():
    return static_file('demo.html', root='.')

@route('/run', method = 'POST')
def process():
    global deepfix
    fixes, repaired, status_codes = deepfix.process([request.forms.get('source_code')])

    result = {}
    result['fixes'] = fixes[0]
    result['repaired'] = repaired[0]

    return json.dumps(result)

if __name__ == "__main__":
    print "Ready!"
    run(host='0.0.0.0', port=8081, server='paste', debug=True)
